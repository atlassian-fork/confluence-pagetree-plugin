package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.internal.ContentPermissionManagerInternal;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.ChildPositionComparator;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.ChildrenAction;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.security.PermissionCheckExemptions;
import com.atlassian.confluence.setup.settings.DarkFeaturesManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ContentCreationComparator;
import com.atlassian.confluence.util.ContentEntityObjectTitleComparator;
import com.atlassian.confluence.util.ContentModificationComparator;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @since 1.0
 */
public class NaturalChildrenAction extends ChildrenAction {

    private static final Logger log = LoggerFactory.getLogger(NaturalChildrenAction.class);

    /**
     * How many elements are displayed on the page tree level
     * The rest of elements will be omitted and "Show all" button(s) will be displayed
     */
    final static int BATCH_SIZE = Integer.getInteger("page-tree.partial-loading-batch-size", 200);

    /**
     * Disables "Show all" feature, so all viewable pages will be rendered
     */
    private final static boolean DISABLE_PARTIAL_LOADING = Boolean.getBoolean("page-tree.partial-loading.disable");

    /* Excerpt key copied from ExcerptMacro */
    private static final String EXCERPT_KEY = "confluence.excerpt";

    private XhtmlContent xhtmlContent;
    private PageManager pageManager;
    private ExcerptHelper excerptHelper;
    private Boolean excerpt;
    private Boolean reverse;
    private String sort;
    private String treeId;
    private Long treePageId;
    private String[] ancestors;

    /**
     * includes ancestors and current page
     */
    private Set<Long> mustBeDisplayedItems = new HashSet<>();
    private String spaceKey;
    private int startDepth;
    private boolean hasRoot;
    private boolean disableLinks;
    private boolean mobile;
    private boolean expandCurrent;
    private String placement;
    private Long elementsAfter;
    private Long elementsBefore;
    private List<Long> idsToExpand = new ArrayList<>();

    /**
     * true in case of Confluence admin (so permissions should not be checked)
     */
    private boolean exempt;

    /**
     * special stack to emulate "local variables" in velocity
     */
    final List<HasMoreInfo> hasMoreStack = new ArrayList();

    public static final String SORT_BITWISE = "bitwise";
    public static final String SORT_CREATION = "creation";
    public static final String SORT_MODIFIED = "modified";
    public static final String SORT_NATURAL = "natural";
    public static final String SORT_POSITION = "position";
    public static final String SORT_DEFAULT = SORT_POSITION;

    private ContentPermissionManagerInternal contentPermissionManager;
    private PermissionCheckExemptions permissionCheckExemptions;

    public void setXhtmlContent(XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
    }

    @Override
    public void setPageManager(PageManager pageManager) {
        super.setPageManager(pageManager);
        this.pageManager = pageManager;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper) {
        this.excerptHelper = excerptHelper;
    }

    // if excerpt is enabled, we can't do anything except
    // loading the entire content of each page every time
    // because "excerpt" field in index contains only first 256 symbols
    // but here we need the content of "excerpt" macro instead
    // we get "light" page as parameter, so we are loading
    // the entire page to get page's content
    public String getPageExcerptHtml(Page page) {
        page = pageManager.getPage(page.getId());
        if (page == null) {
            return "";
        }
        String excerpt = excerptHelper.getExcerpt(page);
        if (StringUtils.isBlank(excerpt)) {
            return "";
        } else {
            PageContext renderContext = page.toPageContext();
            renderContext.pushRenderMode(RenderMode.suppress(RenderMode.F_FIRST_PARA));
            ConversionContext excerptConversionContext = new DefaultConversionContext(renderContext);
            try {
                excerpt = xhtmlContent.convertStorageToView(excerpt, excerptConversionContext);
            } catch (XMLStreamException | XhtmlException e) {
                excerpt = handleError(excerpt, e);
            }
            renderContext.popRenderMode();
            return excerpt;
        }
    }

    private String handleError(final String excerpt, final Exception exception) {
        log.warn("Error rendering wiki link: [" + excerpt + "]", exception);
        return String.format("<span class=\"error\">%s</span>", HtmlUtil.htmlEncode(excerpt));
    }

    public void setExcerpt(Boolean bool) {
        excerpt = bool;
    }

    // PGTR-76 - We need to catch whether the current user has view permission.
    // If not the we'll return a http error response instead of letting the page keep refreshing itself.
    @Override
    public String doDefault() throws Exception {
        if (!super.isPermitted())
            return ERROR;

        if (DISABLE_PARTIAL_LOADING) {
            log.debug("Partial page tree loading is disabled");
        } else {
            log.debug("Partial page tree loading is enabled, batch size: {}", BATCH_SIZE);
        }

        exempt = permissionCheckExemptions.isExempt(AuthenticatedUserThreadLocal.get());

        if (expandCurrent) {
            idsToExpand.add(treePageId);
        }

        return super.doDefault();
    }

    // PGTR-76 - This is needed to get the plugin to return a 403 http error status.
    @Override
    public boolean isPermitted() {
        return true;
    }

    public Boolean getExcerpt() {
        return excerpt;
    }

    public void setTreeId(String tid) {
        treeId = tid;
    }

    public String getTreeId() {
        return treeId;
    }

    public void setAncestors(String[] ancestors) {
        for (String ancestor: ancestors) {
            try {
                mustBeDisplayedItems.add(Long.parseLong(ancestor));
            } catch (Exception e) {
                log.warn("Unable to parse ancestor: " + e.getMessage());
            }
        }
        this.ancestors = ancestors;
    }

    public String[] getAncestors() {
        return this.ancestors;
    }

    public void setStartDepth(int startDepth) {
        this.startDepth = startDepth;
    }

    public int getStartDepth() {
        return this.startDepth;
    }

    public void setReverse(Boolean bool) {
        reverse = bool;
    }

    public void setSort(String sortOrder) {
        sort = sortOrder;
    }

    @Override
    public String getSpaceKey() {
        return this.spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public boolean hasRoot() {
        return this.hasRoot;
    }

    public void setHasRoot(boolean hasRoot) {
        this.hasRoot = hasRoot;
    }

    public boolean getDisableLinks() {
        return disableLinks;
    }

    public void setDisableLinks(boolean disableLinks) {
        this.disableLinks = disableLinks;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }

    public void setTreePageId(Long treePageId) {
        if (treePageId != null) {
            mustBeDisplayedItems.add(treePageId);
        }
        this.treePageId = treePageId;
    }

    public void setExpandCurrent(boolean expandCurrent) {
        this.expandCurrent = expandCurrent;
    }

    public List<Long> getIdsToExpand() {
        return idsToExpand;
    }

    // We do not check inherited permissions here because inherited permissions
    // were already checked in doDefault, which is called before page rendering
    // This method will not be called if user does not have "View" permissions
    // for the space or for the root page (depends on request)
    // Also, as this method is called, it means we already checked permissions for
    // the the provided page and we user has 'View' access for the page
    public List getPermittedChildren(Page page) {
        if (page == null) {
            return Collections.EMPTY_LIST;
        }

        AbstractPage original = super.getPage();
        super.setPage(page);

        List<Page> children = page.getChildren();
        if (!exempt) {
            children = contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(children, getAuthenticatedUser(), ContentPermission.VIEW_PERMISSION);
        }

        List<Page> sortedChildren = new ArrayList<>(children);
        sortList(sortedChildren, sort, reverse);

        super.setPage(original);
        return sortedChildren;
    }

    // we do not check inherited permissions here because they has been already checked in doDefault
    public PageList getLimitedPermittedChildren(Page page) {
        if (page == null) {
            return new PageList();
        }

        AbstractPage original = super.getPage();
        super.setPage(page);

        List<Page> sortedChildren = sortPages(page.getChildren());
        try {
            return limitBeforeAndAfter(sortedChildren);
        } finally {
            super.setPage(original);
        }
    }

    private PageList limitBeforeAndAfter(final List<Page> sortedChildren) {
        final List<Page> limitedPageList = limitByBeforeAndAfterFilter(sortedChildren, elementsBefore, elementsAfter);
        final LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManager, getAuthenticatedUser(), exempt);
        final boolean skipLimit = elementsAfter != null || elementsBefore != null;
        // "Show all" button must not be displayed in the page tree macro
        final boolean sidebarPlacement = "sidebar".equals(placement);
        return (skipLimit || DISABLE_PARTIAL_LOADING || !sidebarPlacement)
                ? loadMoreHelper.getAllPermittedElements(limitedPageList)
                : loadMoreHelper.getSublistWithLoadMoreSupport(limitedPageList, BATCH_SIZE, mustBeDisplayedItems);
    }

    private List<Page> limitByBeforeAndAfterFilter(List<Page> pages, Long elementsBefore, Long elementsAfter) {
        if (elementsAfter != null) {
            for (int i = 0; i < pages.size(); i++) {
                if (elementsAfter.equals(pages.get(i).getId())) {
                    return pages.subList(i + 1, pages.size());
                }
            }
            return Collections.emptyList();
        }

        if (elementsBefore != null) {
            for (int i = 0; i < pages.size(); i++) {
                if (elementsBefore.equals(pages.get(i).getId())) {
                    return pages.subList(0, i);
                }
            }
            return Collections.emptyList();
        }

        return pages;
    }

    private List<Page> sortPages(List<Page> children) {
        List<Page> sortedChildren = new ArrayList<>(children);
        sortList(sortedChildren, sort, reverse);
        return sortedChildren;
    }

    @Override
    public boolean hasPermittedChildren(Page page) {
        if (exempt) {
            return page.getChildren().size() > 0;
        } else {
            return contentPermissionManager.hasVisibleChildren(page, AuthenticatedUserThreadLocal.get());
        }
    }

    public boolean isAncestorPage(Page page) {
        String pageId = page.getIdAsString();
        if (StringUtils.isEmpty(pageId) || ancestors == null || ancestors.length <= 0) return false;

        for (int i = 0; i < ancestors.length; i++) {
            if (pageId.equals(ancestors[i])) return true;
        }
        return false;
    }

    // We do not check inherited permissions here because inherited permissions
    // were already checked in doDefault, which is called before page rendering
    // This method will not be called if user does not have "View" permissions
    // for the space
    public List getAllPermittedStartPages(String spaceKey) {
        if (StringUtils.isEmpty(spaceKey)) {
            return Collections.EMPTY_LIST;
        }

        Space space = spaceManager.getSpace(spaceKey);
        if (space == null) {
            return Collections.EMPTY_LIST;
        }

        // Top level pages contains everything orphan and the space home page.
        List<Page> topLevelPages = pageManager.getTopLevelPages(space);
        if (CollectionUtils.isEmpty(topLevelPages)) {
            return Collections.EMPTY_LIST;
        }
        List<Page> visibleTopLevelPages = contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(topLevelPages, AuthenticatedUserThreadLocal.get(), ContentPermission.VIEW_PERMISSION);
        sortList(visibleTopLevelPages, sort, reverse);
        return visibleTopLevelPages;
    }

    public PageList getLimitedPermittedStartPages(String spaceKey) {
        if (StringUtils.isEmpty(spaceKey)) {
            return new PageList();
        }

        Space space = spaceManager.getSpace(spaceKey);
        if (space == null) {
            return new PageList();
        }

        // Top level pages contains everything orphan and the space home page.
        List<Page> topLevelPages = pageManager.getTopLevelPages(space);
        if (CollectionUtils.isEmpty(topLevelPages)) {
            return new PageList();
        }
        List<Page> sortedPages = sortPages(topLevelPages);
        return limitBeforeAndAfter(sortedPages);
    }

    private void sortList(List<Page> pageList, String sortType, Boolean isReverse) {
        if (SORT_BITWISE.equals(sortType)) {
            pageList.sort(ContentEntityObjectTitleComparator.getInstance());
        } else if (SORT_CREATION.equals(sortType)) {
            pageList.sort(new ContentCreationComparator());
        } else if (SORT_MODIFIED.equals(sortType)) {
            pageList.sort(new ContentModificationComparator());
        } else if (SORT_NATURAL.equals(sortType)) {
            pageList.sort(new NaturalPageComparator());
        } else if (SORT_POSITION.equals(sortType)) {
            pageList.sort(new ChildPositionComparator());
        }

        if (isReverse == Boolean.TRUE) {
            Collections.reverse(pageList);
        }
    }

    @Override
    public void setContentPermissionManager(ContentPermissionManagerInternal contentPermissionManager) {
        this.contentPermissionManager = contentPermissionManager;
    }

    @Override
    public void setPermissionCheckExemptions(PermissionCheckExemptions permissionCheckExemptions) {
        this.permissionCheckExemptions = permissionCheckExemptions;
    }

    public Long getElementsAfter() {
        return elementsAfter;
    }

    public void setElementsAfter(Long elementsAfter) {
        this.elementsAfter = elementsAfter;
    }

    public Long getElementsBefore() {
        return elementsBefore;
    }

    public void setElementsBefore(Long elementsBefore) {
        this.elementsBefore = elementsBefore;
    }

    /**
     * id and nextPrevId could be strings or any numeric types (they are generated in velocity template)
     * so it would be better to use Object types
     * It is suitable because we are going to return these objects back to velocity then
     */
    public void push(Object id, boolean hasMore, Object nextPrevId) {
        hasMoreStack.add(new HasMoreInfo(hasMore, id, nextPrevId));
    }

    public HasMoreInfo pop() {
        return hasMoreStack.remove(hasMoreStack.size() - 1);
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }
}
