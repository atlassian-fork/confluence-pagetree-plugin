package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.pages.Page;

import java.util.Collections;
import java.util.List;

/**
 * Partial page list with additional flags: hasMoreAfter and hasMoreBefore
 *
 * @since 4.0.0
 */
public class PageList {
    /**
     * Demonstrates which "load more" buttons should be displayed (or not displayed at all)
     */
    public enum LoadMoreMode {
        NO_LOAD_MORE_BUTTONS,
        LOAD_MORE_BEFORE_ONLY,
        LOAD_MORE_AFTER_ONLY,
        LOAD_MORE_BOTH_BEFORE_AND_AFTER;

        public boolean hasLoadMoreBefore() {
            return LOAD_MORE_BEFORE_ONLY == this || LOAD_MORE_BOTH_BEFORE_AND_AFTER == this;
        }

        public boolean hasLoadMoreAfter() {
            return LOAD_MORE_AFTER_ONLY == this || LOAD_MORE_BOTH_BEFORE_AND_AFTER == this;
        }
    }

    private final List<Page> pageList;
    private final LoadMoreMode loadMoreMode;

    public PageList() {
        this(Collections.emptyList());
    }

    public PageList(List<Page> pageList) {
        this(pageList, LoadMoreMode.NO_LOAD_MORE_BUTTONS);
    }

    public PageList(List<Page> pageList, LoadMoreMode loadMoreMode) {
        this.pageList = pageList;
        this.loadMoreMode = loadMoreMode;
    }

    public List<Page> getPageList() {
        return pageList;
    }

    public boolean isHasMoreAfter() {
        return loadMoreMode.hasLoadMoreAfter();
    }

    public boolean isHasMoreBefore() {
        return loadMoreMode.hasLoadMoreBefore();
    }

    public Long getFirstLoadedId() {
        return (pageList.size() > 0) ? pageList.get(0).getId() : null;
    }

    public Long getLastLoadedId() {
        return (pageList.size() > 0) ? pageList.get(pageList.size() - 1).getId() : null;
    }
}
