package com.adaptavist.confluence.naturalchildren;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.bonnie.LuceneUtils;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchFieldNames;
import com.atlassian.confluence.search.v2.SearchFilter;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResultType;
import com.atlassian.confluence.search.v2.searchfilter.InSpaceSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.TermSearchFilter;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.user.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.confluence.plugins.pagetree.PageHierarchyExtractor.ANCESTORS_KEY;
import static com.atlassian.confluence.plugins.pagetree.PageHierarchyExtractor.POSITION_KEY;
import static com.atlassian.confluence.plugins.pagetree.PageHierarchyExtractor.UNSPECIFIED_POSITION;

/**
 * @since 4.0.0
 */
public class PageTreeBuilder {
    private static final Logger log = LoggerFactory.getLogger(PageTreeBuilder.class);

    private final SearchManager searchManager;
    private final PageManager pageManager;
    private final PermissionManager permissionManager;

    public PageTreeBuilder(SearchManager searchManager, PageManager pageManager, PermissionManager permissionManager) {
        this.searchManager = searchManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
    }

    private Page buildPage(Map<String, String[]> doc, Space space) {
        Page page = new Page();
        page.setTitle(extractStringValue(doc.get(SearchFieldNames.TITLE)));
        page.setId(extractPageId(doc.get(SearchFieldNames.HANDLE)));
        page.setSpace(space);
        page.setCreationDate(extractDate(doc.get(SearchFieldNames.CREATION_DATE)));
        page.setLastModificationDate(extractDate(doc.get(SearchFieldNames.MODIFIED)));
        page.setPosition(extractPositionValue(doc.get(POSITION_KEY)));
        return page;
    }

    private Date extractDate(String[] values) {
        return LuceneUtils.stringToDate(extractStringValue(values));
    }

    private Integer extractPositionValue(String[] values) {
        // it is an old record, without position field at all
        if (values == null || values.length == 0) {
            return null;
        }

        // here we are processing record with the position field. If it is empty, we have to return 0
        // so the page tree builder will know that record has a position field (and will not retrieve it from DB)
        String value = values[0];
        try {
            return StringUtils.isNotEmpty(value) ? Integer.parseInt(value) : UNSPECIFIED_POSITION;
        } catch (Exception e) {
            // if search index contains broken values, we should not prevent building the page
            return UNSPECIFIED_POSITION;
        }
    }

    // Search manager returns all fields as arrays, but all our properties contain only single values
    private String extractStringValue(String[] values) {
        if (values == null || values.length == 0) {
            return "";
        }
        return values[0];
    }

    // content handles are stored in such format: {content-type}-{id}
    private long extractPageId(String[] values) {
        String value = extractStringValue(values);
        try {
            HibernateHandle hibernateHandle = new HibernateHandle(value);
            return hibernateHandle.getId();
        } catch (ParseException e) {
            log.warn("Unable to parse page id from the handle: {}, message: ", value, e.getMessage());
            return 0;
        }
    }

    private static final HashSet<String> SEARCH_FIELDS = new HashSet<>(Arrays.asList(
            SearchFieldNames.HANDLE,
            SearchFieldNames.TITLE,
            ANCESTORS_KEY,
            SearchFieldNames.CREATION_DATE,
            SearchFieldNames.MODIFIED,
            SearchFieldNames.TYPE,
            POSITION_KEY));

    private PageTree searchAndBuildPageTreeHierarchy(User currentUser, Space space, SearchFilter searchFilter, boolean useSortPosition, Long currentPageId) throws InvalidSearchException {
        final PageTree pageTree = new PageTree();
        Map<Long, Page> allPages = new HashMap<>();

        // when we go through all pages we ensure that all pages
        // have positions
        // if at least one page does not have a position
        // we have to load all pages from DB and update positions
        // it could be useful in two cases:
        // 1. When we use plugin in previous Confluence versions without position field in search index
        // 2. New Confluence is installed but pages are still not reindexed
        AtomicReference<Boolean> hasNullPositions = new AtomicReference<>(false);

        searchManager.scan(searchFilter, SEARCH_FIELDS, doc -> {
            Page page = buildPage(doc, space);
            AncestorList ancestorList = new SearchResultsAncestorList(doc.get(ANCESTORS_KEY));
            pageTree.addPage(ancestorList, page);
            if (page.getPosition() == null && !hasNullPositions.get()) {
                hasNullPositions.set(true);
            }
            allPages.put(page.getId(), page);
        });

        // when we add a page, sometimes index is still not updated
        // so we have to add this page manually in this case
        if (currentPageId != null && !allPages.containsKey(currentPageId)) {
            addExistingPageToTheTree(currentUser, pageTree, currentPageId);
        }

        if (useSortPosition && hasNullPositions.get()) {
            updatePositionsInPages(space, allPages);
        }
        return pageTree;
    }

    private void addExistingPageToTheTree(User currentUser, PageTree pageTree, Long pageId) {
        Page page = pageManager.getPage(pageId);
        if (page == null) {
            return;
        }

        if (!permissionManager.hasPermission(currentUser, Permission.VIEW, page)) {
            return;
        }

        AncestorList ancestorList = new PageAncestorList(page.getAncestors(), page);
        pageTree.addPage(ancestorList, page);
    }

    /**
     * As search index does not have a position field, we have to get required information from DB
     * and update our pages (if sort order is "position")
     */
    private void updatePositionsInPages(Space space, Map<Long, Page> allPages) {
        List<Page> dbPages = pageManager.getPages(space, true);
        for (Page dbPage: dbPages) {
            Page page = allPages.get(dbPage.getId());
            if (page != null) {
                page.setPosition(dbPage.getPosition());
            }
        }
    }

    PageTree buildPageTree(User currentUser, AbstractPage page, boolean useSortPosition, Long currentPageId) throws InvalidSearchException {
        SearchFilter searchFilter = new TermSearchFilter(SearchFieldNames.TYPE, Page.CONTENT_TYPE).and(
                new TermSearchFilter(ANCESTORS_KEY, Long.toString(page.getId())))
                .and(SiteSearchPermissionsSearchFilter.getInstance());

        return searchAndBuildPageTreeHierarchy(currentUser, page.getSpace(), searchFilter, useSortPosition, currentPageId);
    }

    PageTree buildPageTree(User currentUser, Space space, boolean useSortPosition, Long currentPageId) throws InvalidSearchException {
        SearchFilter searchFilter = new TermSearchFilter(SearchFieldNames.TYPE, Page.CONTENT_TYPE).and(
                new InSpaceSearchFilter(Collections.singleton(space.getKey()))
                        .and(new TermSearchFilter(DocumentFieldName.DOCUMENT_TYPE, SearchResultType.CONTENT.name()))
                        .and(SiteSearchPermissionsSearchFilter.getInstance()));

        return searchAndBuildPageTreeHierarchy(currentUser, space, searchFilter, useSortPosition, currentPageId);
    }
}
