package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PageHierarchyExtractorTestCase
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private Document document;

    private Page page;
    private PageHierarchyExtractor pageHierarchyExtractor;

    @Before
    public void setUp() {
        document = new Document();
        page = new Page();
        page.setTitle("Test Page");
        pageHierarchyExtractor = new PageHierarchyExtractor();
    }

    @Test
    public void testPageIdIndexedWithAncestorPageId()
    {
        Page ancestorPage = new Page();
        ancestorPage.setId(12);
        ancestorPage.setTitle("Ancestor Test Page");

        List<Page> ancestorList = new ArrayList<>();
        ancestorList.add(ancestorPage);

        page.setId(123);
        page.setAncestors(ancestorList);

        pageHierarchyExtractor.addFields(document, new StringBuffer(), page);

        assertEquals(2 + ancestorList.size(), document.getFields().size());

        List<String> ids = new ArrayList<>();
        for (IndexableField actualField : document.getFields()) {
            assertThat(actualField.name(), either(is(PageHierarchyExtractor.ANCESTORS_KEY)).or(is(PageHierarchyExtractor.POSITION_KEY)));
            assertTrue(actualField.fieldType().stored());
            assertTrue(actualField.fieldType().indexed());
            ids.add(actualField.stringValue());
        }
        assertThat(ids, hasItems("12", "123"));
    }

    @Test
    public void testAttachmentPageIndexed()
    {
        page.setId(321);

        Attachment attachment = new Attachment();
        attachment.setContainer(page);

        pageHierarchyExtractor.addFields(document, new StringBuffer(), attachment);

        assertEquals(1, document.getFields().size());
    }

    @Test
    public void testAttachmentNotIndexed()
    {
        Attachment attachment = new Attachment();
        pageHierarchyExtractor.addFields(document, new StringBuffer(), attachment);
        assertEquals(0, document.getFields().size());
    }

    @Test
    public void testBlogPostNotIndexed()
    {
        BlogPost blogPost = new BlogPost();
        pageHierarchyExtractor.addFields(document, new StringBuffer(), blogPost);
        assertEquals(0, document.getFields().size());
    }
}
