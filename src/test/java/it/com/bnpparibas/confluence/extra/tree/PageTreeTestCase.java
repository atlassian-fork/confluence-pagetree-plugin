package it.com.bnpparibas.confluence.extra.tree;

import it.com.atlassian.confluence.plugins.pagetree.AbstractConfluencePluginWebTestCaseBase;

import java.util.ArrayList;

public class PageTreeTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private String spaceKey = "tst";
    private String spaceTitle = "Test";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        generateTestDataInTestSpace();
    }

    @Override
    protected void tearDown() throws Exception {
        deleteSpace(spaceKey);
        super.tearDown();
    }

    private void generateTestDataInTestSpace() {
        createSpace(spaceKey, spaceTitle, "");
        createPageInSpaceHomePage(spaceKey, spaceTitle,"testPageTree", "{pagetree}");
        createPageInSpaceHomePage(spaceKey, spaceTitle,"testPageTreeTwo", "{pagetree}");
    }

    public void testPageTreeRootPointingToSpaceHomePage() {
        gotoPage("/display/" + spaceKey + "/" + spaceTitle + "+Home");

        String homePageId = getElementAttributeByXPath("//meta[@name='ajs-page-id']", "content");

        final long pageId = createPage(spaceKey, "testRenderedPageTreeFromSpaceHomePage", "{pagetree}");

        getIndexHelper().update();

        viewPageById(pageId);

        assertPageTreeElements("", "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=", pageId, "false", Long.parseLong(homePageId), "", "0", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + pageId);

        gotoPage(generateNaturalChildrenActionPath());

        assertEquals("testPageTree", getElementTextByXPath("//ul//li[1]//span//a"));
        assertEquals("testPageTreeTwo", getElementTextByXPath("//ul//li[2]//span//a"));
        assertElementNotPresentByXPath("//ul//li[3]//span//a");
    }

    public void testPageTreeRootPointingToSelfAndSortByBitwiseInReverseOrder() {
        final long parentPageId = createPage(spaceKey, "testPageTreeRootPointingToParentSortByBitwiseInReverseOrder", "{pagetree:root=@self|sort=bitwise|excerpt=true|reverse=true}");

        final long childPageId = createPage(spaceKey, "Apple", "{pagetree:root=@self|sort=bitwise|excerpt=true|reverse=true}", parentPageId);
        createPage(spaceKey, "Banana", "{pagetree:root=testSortByBitwiseInReverseOrder|sort=bitwise|excerpt=true|reverse=true}", parentPageId);

        getIndexHelper().update();

        viewPageById(parentPageId);

        assertPageTreeElements("", "decorator=none&excerpt=true&sort=bitwise&reverse=true&disableLinks=false&expandCurrent=false&placement=", parentPageId, "false", parentPageId, "", "0", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + parentPageId);

        gotoPage(generateNaturalChildrenActionPath());

        assertEquals("Banana", getElementTextByXPath("//ul//li[1]//span//a"));
        assertEquals("Apple", getElementTextByXPath("//ul//li[2]//span//a"));

        viewPageById(childPageId);

        assertPageTreeElements("", "decorator=none&excerpt=true&sort=bitwise&reverse=true&disableLinks=false&expandCurrent=false&placement=", childPageId, "false", childPageId, "", "0", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPageId);
    }

    public void testPageTreeExpandCurrent() {
        final long root = createPage(spaceKey, "testPageTreeRootPointingToSelfAndExpandCurrent","Root");

        final long parentPageId = createPage(spaceKey, "Parent", "{pagetree:root=@parent|expandCurrent=true}", root);
        final long childPageId = createPage(spaceKey, "Child", "Child content", parentPageId);

        getIndexHelper().update();

        viewPageById(parentPageId);

        assertPageTreeElements("", "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=true&placement=", parentPageId, "false", root, "", "0", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + parentPageId);

        gotoPage(generateNaturalChildrenActionPath());

        assertEquals("Child", getElementTextByXPath("//*[@id='childrenspan" + childPageId + "-0']//a"));
    }

    public void testPageTreeRootPointingToParent() {
        final long parentPageId = createPage(spaceKey, "ParentPage", "Parent Page Content");
        final long childPageId = createPage(spaceKey, "testPageTreeRootPointingToParent",
                "{pagetree:root=@parent|startDepth=1}", parentPageId);

        getIndexHelper().update();

        viewPageById(childPageId);

        assertPageTreeElements("", "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=", childPageId, "false", parentPageId, "", "1", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPageId);

        gotoPage(generateNaturalChildrenActionPath());

        assertEquals("testPageTreeRootPointingToParent", getElementTextByXPath("//ul//li[1]//span//a"));
    }

    public void testRenderedSearchBoxAndExpandCollapseLink() {
        final long parentPageId = createPage(spaceKey, "ParentPage", "ParentContent");
        final long childPageId = createPage(spaceKey, "ChildPage", "{pagetree:root=@self|searchBox=true|expandCollapseAll=true|startDepth=2}", parentPageId);

        getIndexHelper().update();

        viewPageById(childPageId);

        assertElementPresentByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@name='queryString']");
        assertElementPresentByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@type='submit']");
        assertEquals("Expand all", getElementTextByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='plugin_pagetree_expandcollapse']//a[@class='plugin_pagetree_expandall']"));
        assertEquals("Collapse all", getElementTextByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='plugin_pagetree_expandcollapse']//a[@class='plugin_pagetree_collapseall']"));
        assertPageTreeElements("", "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=", childPageId, "false", childPageId, "", "2", "Loading...", "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPageId);
    }

    public void testSearchBoxSearchResults() {
        setConfluenceBaseUrl(getSpaceHelper(spaceKey).getConfluenceWebTester().getBaseUrl());

        final long parentPageId = createPage(spaceKey, "ParentPage", "ParentContent");
        final long childPageId = createPage(spaceKey, "ChildPage", "{pagetree:root=@parent|searchBox=true}", parentPageId);

        getIndexHelper().update();

        viewPageById(childPageId);

        String ancestorId = getElementAttributeByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//fieldset//input[@name='ancestorId']", "value");

        assertElementPresentByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form//input[@type='text']");
        assertElementPresentByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form//input[@type='submit']");

        setWorkingForm("pagetreesearchform");
        setTextField("queryString", "ParentPage");
        clickElementByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form//input[@type='submit']");

        assertEquals("ancestorIds:" + ancestorId + " AND ParentPage",
                getElementAttributeByXPath("//input[@id='query-string']", "value"));

        assertEquals("ParentPage",
                getElementTextByXPath("//ol[starts-with(@class, 'search-results') and not(starts-with(@class, 'search-results-'))]/li[1]//h3/a"));

        clickLinkWithText("ParentPage");
        assertEquals(spaceKey,
                getElementAttributeByXPath("//meta[@id='confluence-space-key']", "content")
        );
    }

    // PGTR-64
    public void testRootPageNotFoundErrorMessage() {
        final long page = createPage(spaceKey, "testRootPageNotFoundErrorMessage", "{pagetree:root=<script>alert('12')</script>}");

        getIndexHelper().update();

        viewPageById(page);

        assertEquals("The root page <script>alert('12')</script> could not be found in space Test.", getElementTextByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='error']//span[@class='errorMessage']").trim());
    }

    public void testExcerptWithWikiMarkup() {
        gotoPage("/display/" + spaceKey + "/" + spaceTitle + "+Home");

        String homePageId = getElementAttributeByXPath("//meta[@name='ajs-page-id']", "content");
        createPage(spaceKey, Integer.parseInt(homePageId), "testExcerptWithWiki", "{excerpt}<h1>*text*</h1>{excerpt}", new ArrayList());
        final long page = createPage(spaceKey, Integer.parseInt(homePageId), "testTreeWithWiki", "{pagetree:excerpt=true}", new ArrayList());
        getIndexHelper().update();

        viewPageById(page);
        gotoPage(generateNaturalChildrenActionPath());
        assertTrue(getTestingEngine().getPageSource().contains("<span class='smalltext'> &lt;h1&gt;<strong>text</strong>&lt;/h1&gt;</span>"));
    }

    private String generateNaturalChildrenActionPath() {
        String treeRequestId = getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treeRequestId']", "value");
        treeRequestId = treeRequestId.replaceAll("&amp;", "&");

        final String hasRoot = Boolean.toString(getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='noRoot']", "value").equals("false"));

        String pageId = getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='rootPageId']", "value");

        String treeId;
        if (getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]", "value").length() == 0) {
            treeId = "0";
        } else {
            treeId = getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]", "value");
        }

        String startDepth = getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='startDepth']", "value");
        String ancestorId = getElementAttributeByXPath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//fieldset//input[@name='ancestorId']", "value");
        String treePageId = getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treePageId']", "value");

        String link = treeRequestId + "&hasRoot=" + hasRoot + "&pageId=" + pageId + "&treeId=" + treeId + "&startDepth=" + startDepth + "&ancestors=" + ancestorId + "&treePageId=" + treePageId;
        link = link.substring(link.indexOf("/plugins"));

        return link;
    }

    private void assertPageTreeElements(String treeId, String treeRequest, long treePageId,
            String noRoot, long rootPageId, String rootPage, String startDepth, String internationalizationText, String loginPath) {
        String baseXPath = "//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]";
        String contextPath = getElementAttributeByXPath("//meta[@id='confluence-context-path']", "content");

        assertElementPresentByXPath(baseXPath);
        assertElementPresentByXPath(baseXPath + "//ul//div[@class='plugin_pagetree_children']");
        assertElementPresentByXPath(baseXPath + "//fieldset");

        assertEquals(treeId,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='treeId']", "value"));
        assertEquals(contextPath + "/plugins/pagetree/naturalchildren.action?" + treeRequest,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='treeRequestId']", "value"));
        assertEquals(Long.toString(treePageId),
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='treePageId']", "value"));

        assertEquals(noRoot,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='noRoot']", "value"));
        assertEquals(Long.toString(rootPageId),
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='rootPageId']", "value"));

        assertEquals(rootPage,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='rootPage']", "value"));
        assertEquals(startDepth,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='startDepth']", "value"));
        assertEquals(spaceKey,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='spaceKey']", "value"));

        assertEquals(internationalizationText,
                getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='i18n-pagetree.loading']", "value"));

        String actualValue = getElementAttributeByXPath(baseXPath + "//fieldset//input[@name='loginUrl']", "value");
        if (actualValue.endsWith("&permissionViolation=true")) { // 5.9.1
            actualValue = actualValue.substring(0, actualValue.indexOf("&permissionViolation=true"));
        }
        assertEquals(contextPath + "/login.action?os_destination=" + loginPath, actualValue);
    }
}
